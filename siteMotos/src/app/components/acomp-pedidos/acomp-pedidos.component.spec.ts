import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcompPedidosComponent } from './acomp-pedidos.component';

describe('AcompPedidosComponent', () => {
  let component: AcompPedidosComponent;
  let fixture: ComponentFixture<AcompPedidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcompPedidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcompPedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
